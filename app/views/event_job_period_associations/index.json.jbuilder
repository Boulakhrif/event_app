json.array!(@event_job_period_associations) do |event_job_period_association|
  json.extract! event_job_period_association, :id, :event_id, :job_id, :period_id, :job_end
  json.url event_job_period_association_url(event_job_period_association, format: :json)
end
