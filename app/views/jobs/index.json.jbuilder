json.array!(@jobs) do |job|
  json.extract! job, :id, :event_id, :name, :jobtext, :processing_time, :ressource_demand
  json.url job_url(job, format: :json)
end
